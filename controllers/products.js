//[SECTION] Dependencies and Modules
const Product = require('../models/Product');

//[SECTION] Functionality [CREATE]
module.exports.createProduct = async (req, res) => {
    let prodN= req.body.name;
    let desc = req.body.description;
    let amt = req.body.price;
    let ctg = req.body.category;
    let stck = req.body.stock;

    if (!prodN.trim() || !desc.trim() || !ctg.trim() || parseInt(amt) <=0 ) {
        return res.send('Invalid Inputs.');
    } 

    Product.findOne({$and:[{name:prodN},{description: desc}]}).then (prodFound =>{
       if (prodFound != null && prodFound.name === prodN && prodFound.description === desc){
            return res.send('Product already existed, try again!');
           
            }
     else{ 
            let newProduct = new Product({
            name: prodN,
            description: desc,
            price: amt,
            category: ctg,
            stock: stck,
            createdBy: req.user.id
        })
            return newProduct.save().then((savedProd, error) => {
            if (error) {
                return res.send('Failed to add new product');
            } else {
                return res.send(savedProd);
            }
        })
        .catch(err => res.send(err));
  }
    })
    .catch(err => res.send(err)); 
};


//[SECTION] Functionality [RETRIEVE]
module.exports.allProducts = async (req, res) => {
    Product.find({status:'available'}).then(result=> 
        res.send(result))
    .catch(err => res.send(err)) 
};

module.exports.singleProduct = async (req, res) => {
    Product.findById(req.params.id).then(resultOneProduct => {
        return res.send(resultOneProduct);
    });
};

module.exports.singleProductReviews = async (req, res) => {
    Product.findById(req.params.id).then(resultOneProduct => {
        return res.send(resultOneProduct.reviews);
    });
};

//[SECTION] Functionality [UPDATE]
module.exports.updateProduct = async (req, res) => {
    let prodN= req.body.name;
    let desc = req.body.description;
    let amt = req.body.price;
    let cat=req.body.category;
    let stck=req.body.stock;

    let updatedProd = {
          name: prodN,
          description: desc,
          price: amt,
          category: cat,
          stock: stck
    }
    
    if (!prodN.trim() || !desc.trim() || parseInt(amt)<=0 || !cat.trim() || !stck.trim()){
        return res.send('Invalid Inputs.');
    } 

    Product.findOne({$and:[{name:prodN},{description: desc}]}).then (prodFound =>{
       
       if (prodFound!==null && prodFound.id !== req.params.id && prodFound.name == prodN && prodFound.description == desc){
            return res.send('Product already existed, try again!');
        }else{
            Product.findByIdAndUpdate(req.params.id, updatedProd).then((productUpdated, err) => {
                if (err) {
                    return res.send('Error updating product.');
                } else {
                    return res.send('Product succesfully updated.');
                }
            })
        .catch(err => res.send(err));
       }

    }).catch(err => res.send(err));  
}


module.exports.archivedProduct = async (req, res) => {
     Product.findById(req.params.id).then(resultOneProduct => {
        if(resultOneProduct.status=='archived'){
            return res.send('This product has already been archived.');
        }else{
            Product.findByIdAndUpdate(req.params.id,{status: 'archived'}).then((prodArchived,err)=>{
                if (err) {
                    return res.send('Error deactivating product.');
                } else {
                    return res.send(`Product "${prodArchived.name} -- ${prodArchived.description}" has been archived.`);
                }
            }).catch(err => res.send(err));
        }
        
    });    
}

module.exports.restoreProduct = async (req, res) => {
     Product.findById(req.params.id).then(resultOneProduct => {
        if(resultOneProduct.status=='available'){
            return res.send('This product is already available.');
        }else{
            Product.findByIdAndUpdate(req.params.id,{status: 'available'}).then((prodAvail,err)=>{
                if (err) {
                    return res.send('Error restoring product.');
                } else {
                    return res.send(`Product "${prodAvail.name} -- ${prodAvail.description}" has been restored.`);
                }
            }).catch(err => res.send(err));
        }
        
    });    
}

//[SECTION] Functionality [DELETE]