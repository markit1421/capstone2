// Packages and dependencies
const mongoose = require('mongoose');
const express = require('express');
const dotenv = require('dotenv');
const userRoutes = require('./routes/users'); 
const productRoutes = require('./routes/products');
const orderRoutes = require('./routes/orders');

// Server setup
const app = express();
app.use(express.json());
dotenv.config();
const con = process.env.CONNECTION_STRING;
const port = process.env.PORT;

//[SECTION] Application Routes
app.use('/users',userRoutes); 
app.use('/products',productRoutes); 
app.use('/orders',orderRoutes); 

// Database connection
mongoose.connect(con);

let dbStatus = mongoose.connection;

dbStatus.on('open', () => console.log('Database connection successful!'));

//[SECTION] Gateway Response
app.get('/',(req,res)=>{
	res.send("Welcome to Mark-IT-Check Ecommerce API!");
})

app.listen(port, () => console.log(`Server is running on ${port}`));



