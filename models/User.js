// Dependencies and Modules
const mongoose = require('mongoose');


// Blueprint schema
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    isActive:{
        type: Boolean,
        default: true
    }
});


// Model
const User = mongoose.model("User", userSchema);
module.exports = User;