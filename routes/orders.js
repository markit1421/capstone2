//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/orders');
const auth = require('../auth');

//Destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/new', verify, controller.createOrder);






//[SECTION] Export Route System
module.exports = route;